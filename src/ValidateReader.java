import java.util.Scanner;

public class ValidateReader {
    private final Scanner scan = new Scanner(System.in);
    int ourTurn;

    public int readNumber() {
        System.out.print("Введите ваше значение\n\t1 Камень\n\t2 Ножницы\n\t3 Бумага\nВаш ввод: ");
        ourTurn = scan.nextInt();

        while (ourTurn < 1 || ourTurn > 3) {
            System.out.println("Вы ввели неправильное значение! Повторите попытку!");
            ourTurn = scan.nextInt();
        }

        return ourTurn;
    }

    public int readHardNumber() {
        System.out.print("Введите ваше значение\n\t1 Камень\n\t2 Ножницы\n\t3 Бумага\n\t4 Ящерица\n\t5 Спок\nВаш ввод: ");
        ourTurn = scan.nextInt();

        while (ourTurn < 1 || ourTurn > 5) {
            System.out.println("Вы ввели неправильное значение! Повторите попытку!");
            ourTurn = scan.nextInt();
        }

        return ourTurn;
    }
}
