import java.util.Random;

public class HardGame {
    private final String[] variants = {"Камень", "Ножницы", "Бумага", "Ящерица", "Спок"};

    private final Random rand = new Random();

    private final ValidateReader reader = new ValidateReader();

    private final Rock rock = new Rock();
    private final Scissors scissors = new Scissors();
    private final Paper paper = new Paper();
    private final Lizard lizard = new Lizard();
    private final Spock spock = new Spock();

    public static int winnerCount;
    public static int defeatCount;
    public static int drawCount;
    public static int gameCount;

    public void playGame() {
        int playerVariant = reader.readHardNumber();
        playerVariant -= 1;
        int computerVariant = computerChoice();

        findWinner(playerVariant, computerVariant);
    }

    public void findWinner(int player, int computer) {
        if (variants[player].equals(getComputerVariant(computer))) {
            System.out.print("\nНичья\n\tВыбор игрока: " + variants[player] + "\n\tВыбор компьютера: " + getComputerVariant(computer) + "\n");

            drawCount += 1;
            gameCount += 1;
        } else if (
                getComputerVariant(computer).equals(variants[0]) && variants[player].equals(variants[1])
                        || getComputerVariant(computer).equals(variants[0]) && variants[player].equals(variants[3])
                        || getComputerVariant(computer).equals(variants[1]) && variants[player].equals(variants[2])
                        || getComputerVariant(computer).equals(variants[1]) && variants[player].equals(variants[3])
                        || getComputerVariant(computer).equals(variants[2]) && variants[player].equals(variants[0])
                        || getComputerVariant(computer).equals(variants[2]) && variants[player].equals(variants[4])
                        || getComputerVariant(computer).equals(variants[3]) && variants[player].equals(variants[2])
                        || getComputerVariant(computer).equals(variants[3]) && variants[player].equals(variants[4])
                        || getComputerVariant(computer).equals(variants[4]) && variants[player].equals(variants[0])
                        || getComputerVariant(computer).equals(variants[4]) && variants[player].equals(variants[1])
        ) {
            System.out.print("\nПобедил компьютер\n\tВыбор игрока: " + variants[player] + "\n\tВыбор компьютера: " + getComputerVariant(computer) + "\n");

            defeatCount += 1;
            gameCount += 1;
        } else {
            System.out.print("\nПобедил игрок\n\tВыбор игрока: " + variants[player] + "\n\tВыбор компьютера: " + getComputerVariant(computer) + "\n");

            winnerCount += 1;
            gameCount += 1;
        }
    }

    public int computerChoice() {
        return rand.nextInt(5);
    }

    public String getComputerVariant(int number) {
        String variant = null;

        if (number == 0) {
            variant = rock.getValue();
        } else if (number == 1) {
            variant = scissors.getValue();
        } else if (number == 2) {
            variant = paper.getValue();
        } else if (number == 3) {
            variant = lizard.getValue();
        } else if (number == 4) {
            variant = spock.getValue();
        }

        return variant;
    }

    public int getWinnerCount() {
        return winnerCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public int getGameCount() {
        return gameCount;
    }

    public int getDefeatCount() {
        return defeatCount;
    }
}
