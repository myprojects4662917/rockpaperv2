import java.util.Scanner;

public class Main {
    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        Playing playing = new Playing();

        String[] games = {"easy", "hard"};

        String gameChoice;

        while (true) {
            System.out.print("Выберите сложность игры\n\teasy - легко\n\thard - тяжело\nВведите: ");
            gameChoice = scan.nextLine();

            if (gameChoice.equals(games[0])) {
                playing.play(1);
                break;
            } else if (gameChoice.equals(games[1])) {
                playing.play(2);
                break;
            } else {
                System.out.println("Ошибка ввода! попробуйте ещё раз");
            }
        }

        scan.close();
    }
}