import java.util.Scanner;

public class Playing {
    private final Scanner scan = new Scanner(System.in);

    Game game = new Game();
    HardGame hardGame = new HardGame();

    public void play(int complexity) {
        System.out.print("\nВведите Y чтобы играть или N чтобы закрыть: ");
        String action = scan.nextLine();

        while (true) {
            if (action.equals("Y")) {

                if (complexity == 1) {
                    game.playGame();
                } else if (complexity == 2) {
                    hardGame.playGame();
                }

                System.out.print("\nВведите Y чтобы играть или N чтобы закрыть: ");
                action = scan.nextLine();
            } else if (action.equals("N")) {
                break;
            } else {
                System.out.println("Ошибка ввода");
                action = scan.nextLine();
            }
        }

        if (complexity == 1) {
            winnerPercent(game.getWinnerCount(), game.getDefeatCount(), game.getDrawCount(), game.getGameCount());
        } else if (complexity == 2) {
            winnerPercent(hardGame.getWinnerCount(), hardGame.getDefeatCount(), hardGame.getDrawCount(), hardGame.getGameCount());
        }
    }

    public void winnerPercent(int win, int defeat, int draw, int games) {
        String format = "| %7d  | %10d  | %6d  |  %9d  | %14.2f  |\n";

        double winPercent = ((double) win * 100) / (double) games;

        System.out.println("\n+" + "-".repeat(66) + "+");
        System.out.println("|  Победа  |  Поражение  |  Ничья  |  Всего игр  |  Процент побед  |");
        System.out.println("+" + "-".repeat(66) + "+");
        System.out.printf(format, win, defeat, draw, games, winPercent);
        System.out.println("+" + "-".repeat(66) + "+");
    }
}
